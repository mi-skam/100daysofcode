'''Create a Pomodoro style stopwatch. Each cycle will be a (configurable)
   amount of time long and will notify the user after it ended.
'''
import time

from datetime import datetime
from datetime import timedelta


class Pomodoro:
    def __init__(self, minutes=25):
        self.minutes = timedelta(seconds=minutes*60)
        self.counter = 0

    def run(self):
        t_start = datetime.now()
        t_stop = t_start + self.minutes
        print(f"The Pomodoro timer will end at {t_stop.strftime('%H:%M')} in {self.minutes.total_seconds() // 60} minutes")
        while True:
            if t_stop >= datetime.now():
                # do stuff
                time.sleep(1)
            else:
                print("finished")
                self.counter += 1
                break


p0 = Pomodoro(minutes=1)
p0.run()
